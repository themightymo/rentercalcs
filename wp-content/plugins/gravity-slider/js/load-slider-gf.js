jQuery(function($) {
    if( $('#gform_wrapper_5').length ){
        var select = $( "#input_5_2" );
        var slider = $( "<div id='slider'></div>" ).insertAfter( select ).slider({
            min: 1,
            max: 11,
            range: "min",
            step: .0001,
            animate: "slow",
            value: select[ 0 ].selectedIndex + 1,
            slide: function( event, ui ) {
                select[ 0 ].selectedIndex = ui.value - 1;

                //Calculation for New Weekly Service $ Goal
                var value1 = jQuery('#input_5_7').attr('value');
                var value3 = Number(value1.replace(/[^0-9\.-]+/g,""));
                var value2 = jQuery('#input_5_2').attr('value');
                var finalv = '$' + (value2 * value3);
                var grossv= '$' + (value2 * value3 * 50);
                jQuery("#input_5_6").val(finalv);
                jQuery("#input_5_20").val(grossv);
                //end

                //Calculation for LET'S BREAK IT DOWN section
                var finalv2 = (value2 * value3);
                var value4 = jQuery('#input_5_12').attr('value');
                if ( value4=="" ) {
                    var finalv3 = '$0.00';
                    var value5 = '$0.00';
                    var value7 = '$0.00';
                    var value4 = 0;
                } else {
                    var finalv3v1= (finalv2 / value4);
                    var finalv3v2= Math.trunc(finalv3v1);
                    var finalv3 = '$' + (finalv3v2);
                    var value5v1 = (finalv3v2 * 0.20);
                    var value5v2 = Math.trunc(value5v1);
                    var value5 = '$' + (value5v2);
                    var value6 = ((finalv2 / value4) * 0.20);
                    var value6v1 = (value6 / 2)
                    var value6v2 = Math.trunc(value6v1);
                    var value7 = '$' + (value6v2);
                    var finalv4 = '$' + (value6v2 * value4 * 50);
                }
                jQuery("#input_5_13").val(finalv3);
                jQuery("#input_5_14").val(value5);
                jQuery("#input_5_15").val(value7);
                jQuery("#input_5_19").val(finalv4);
                //end
            }
        });
        $( "#input_5_2" ).on( "change", function() {
            slider.slider( "value", this.selectedIndex + 1 );

            //Calculation for New Weekly Service $ Goal
            var value1 = jQuery('#input_5_7').attr('value');
            var value3 = Number(value1.replace(/[^0-9\.-]+/g,""));
            var value2 = jQuery('#input_5_2').attr('value');
            var finalv = '$' + (value2 * value3);
            var grossv= '$' + (value2 * value3 * 50);
            jQuery("#input_5_6").val(finalv);
            jQuery("#input_5_20").val(grossv);
            //end

            //Calculation for LET'S BREAK IT DOWN section
            var finalv2 = (value2 * value3);
            var value4 = jQuery('#input_5_12').attr('value');
            if ( value4=="" ) {
                var finalv3 = '$0.00';
                var value5 = '$0.00';
                var value7 = '$0.00';
                var value4 = 0;
            } else {
                var finalv3v1= (finalv2 / value4);
                var finalv3v2= Math.trunc(finalv3v1);
                var finalv3 = '$' + (finalv3v2);
                var value5v1 = (finalv3v2 * 0.20);
                var value5v2 = Math.trunc(value5v1);
                var value5 = '$' + (value5v2);
                var value6 = ((finalv2 / value4) * 0.20);
                var value6v1 = (value6 / 2)
                var value6v2 = Math.trunc(value6v1);
                var value7 = '$' + (value6v2);
                var finalv4 = '$' + (value6v2 * value4 * 50);
            }
            jQuery("#input_5_13").val(finalv3);
            jQuery("#input_5_14").val(value5);
            jQuery("#input_5_15").val(value7);
            jQuery("#input_5_19").val(finalv4);
            //end
        });
        $('#input_5_3').on( "change", function() {
            var profit1 = jQuery('#input_5_3').attr('value');
            var profit2 = Math.trunc(profit1);
            slider.slider( "value", profit2+.5 );
            jQuery("#input_5_2").val(profit2);
        });
        /*$('#input_5_8').on( "keyup", function() {
            var week1 = jQuery('#input_5_8').attr('value');
            var week2= Number(week1.replace(/[^0-9\.-]+/g,""));
            var week3 = '$' + (week2);
            jQuery("#input_5_6").val(week3);
        });*/
    }
} );